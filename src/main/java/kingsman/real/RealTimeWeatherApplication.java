package kingsman.real;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RealTimeWeatherApplication {

    public static void main(String[] args) {
        SpringApplication.run(RealTimeWeatherApplication.class, args);
        while (true) {
            try {
                Thread.sleep(10000); // Sleep for a while (e.g., 10 seconds) to prevent high CPU usage
            } catch (InterruptedException e) {
                // Handle the interruption appropriately
                System.err.println("Application interrupted: " + e.getMessage());
                break; // Exit the loop if the thread is interrupted
            }
        }
    }

}
